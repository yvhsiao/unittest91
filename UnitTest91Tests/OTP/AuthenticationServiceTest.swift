//
//  AuthenticationServiceTest.swift
//  UnitTest91Tests
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import XCTest
@testable import UnitTest91

final class AuthenticationServiceTest: XCTestCase {
    
    func test_is_valid() {
        let authenticationService = AuthenticationService()
        let isValid = authenticationService.isValid(account: "joey",
                                                    passcode: "91000000")
        XCTAssertTrue(isValid)
    }
}
