//
//  AuthenticationService.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class AuthenticationService {
    
    func isValid(account: String, passcode: String) -> Bool {
        
        // 根據 account 取得自訂密碼
        let profileDao = ProfileDao()
        let passwordFromDao = profileDao.getPassword(account: account) ?? "Unknown"
        
        // 根據 account 取得 RSA token 目前的亂數
        let rsaTokenDao = RsaTokenDao()
        let randomCode = rsaTokenDao.getRandom(account: account)
        
        // 驗證傳入的 password 是否等於自訂密碼 + RSA token亂數
        let validPassCode = passwordFromDao + randomCode
        let isValid = passcode == validPassCode
        
        return isValid
    }
}
