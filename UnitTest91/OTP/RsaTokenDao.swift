//
//  RsaTokenDao.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class RsaTokenDao {
    
    /// 生成 6 位隨機數
    func getRandom(account: String) -> String {
        let seed = Date().seed
        let result = String(format: "%06d", seed.nextInt(upperBound: 1000000))
        print("randomCode: \(result)")
        return result
    }
}
