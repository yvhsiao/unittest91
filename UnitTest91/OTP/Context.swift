//
//  Context.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class Context {
    
    static let profiles: [String: String] = [
        "joey": "91",
        "mei": "99"
    ]
    
    static func getPassword(key: String) -> String? {
        return profiles[key]
    }
}
