//
//  ProfileDao.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class ProfileDao {
    
    func getPassword(account: String) -> String? {
        return Context.getPassword(key: account)
    }
}
