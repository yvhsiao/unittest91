//
//  Order.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class Order {
    
    private var productName : String
    private var type        : String
    private var price       : Int
    private var customerName: String
    
    init(productName : String? = nil,
         type        : String? = nil,
         price       : Int? = nil,
         customerName: String? = nil) {
        self.productName  = (productName ?? "Unknown")
        self.type         = type ?? "Unknown"
        self.price        = price ?? .zero
        self.customerName = customerName ?? "Unknown"
    }
    
    func getType() -> String {
        return type
    }
    
    func setType(_ type: String) {
        self.type = type
    }
    
    func getPrice() -> Int {
        return price
    }
    
    func setPrice(_ price: Int) {
        self.price = price
    }
    
    func getProductName() -> String {
        return productName
    }
    
    func setProductName(_ name: String) {
        self.productName = name
    }
    
    func getCustomerName() -> String {
        return customerName
    }
    
    func setCustomerName(_ name: String) {
        self.customerName = name
    }
}
