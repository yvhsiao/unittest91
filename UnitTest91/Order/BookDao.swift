//
//  BookDao.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class BookDao {
    
    func insert(order: Order) throws {
        throw BookDaoError.unsupportedOperation
    }
}

// MARK: - Error
enum BookDaoError: Error {
    case unsupportedOperation
}
