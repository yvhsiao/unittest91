//
//  OrderService.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import Foundation

class OrderService {
    
    func syncBookOrders() {
        let orders = getOrders()
        
        // only get orders of book
        let ordersOfBook = orders.filter{ $0.getType() == "Book" }
        
        let bookDao = BookDao()
        for order in ordersOfBook {
            try? bookDao.insert(order: order)
        }
    }
    
    /// parse csv file to get orders
    private func getOrders() -> [Order] {
        guard let filePath = Bundle.main.path(forResource: "order", ofType: "csv") else {
            return []
        }
        let csvString = (try? String(contentsOfFile: filePath)) ?? ""
        var rowsData = csvString.components(separatedBy: "\n")
        rowsData.removeFirst()
        return rowsData.map(orderMapper)
    }
    
    private var orderMapper: ((String) -> Order) {
        return { rowText in
            let columnsData = rowText.components(separatedBy: ",")
            return Order(productName: columnsData[safe: 0],
                         type: columnsData[safe: 1],
                         price: Int(columnsData[safe: 2] ?? "0"),
                         customerName: columnsData[safe: 3]
            )
        }
    }
}
