//
//  Date+GKMersenneTwisterRandomSource.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/18.
//

import GameplayKit

extension Date {
    
    var seed: GKMersenneTwisterRandomSource {

        // 使用當前時間的毫秒數作為Seed創建 GKMersenneTwisterRandomSource
        let milliseconds = Int64(timeIntervalSince1970 * 1000)
        return GKMersenneTwisterRandomSource(seed: .init(milliseconds & 0x0000FFFF))
    }
}
