//
//  Collection+Unwrap.swift
//  UnitTest91
//
//  Created by Yvonne Hsiao on 2024/4/19.
//

import Foundation

extension Collection {

    subscript(safe index: Index) -> Iterator.Element? {
        indices.contains(index) ? self[index] : nil
    }
}
